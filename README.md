A bunch of utility functions for making API request to Glasshouse and LL


Set the Glasshouse JWT secret using the env variable `GLASSHOUSE_JWT_SECRET`. It isn't included as it means this can be used in client side code


# To add to a repo

`yarn add https://gitlab.com/sproutlabs/gh-js-utils.git`
