import superagent from 'superagent';
import jwt from 'jsonwebtoken';
import Debug from 'debug';
import { apiJWT } from '../config';
import 'isomorphic-fetch';
import ApolloClient, { createNetworkInterface } from 'apollo-client';
import gql from 'graphql-tag';

const debug = Debug('gh:utils:ghAPI');

if (!global.window) {
  if (!process.env.GLASSHOUSE_JWT_SECRET) {
    console.warn('You are in a Node env and the env variable GLASSHOUSE_JWT_SECRET has not been set');
  }
  if (!process.env.GLASSHOUSE_BACKEND_SERVICE) {
    console.warn('Looks like you are in a node environment but GLASSHOUSE_BACKEND_SERVICE is not set ');
  }
}
export function getJWTaudFromURL(siteURL) {
  return /^((?:http|https):\/\/(.+?))(\/|$)/.exec(siteURL)[2];
}
function buildAPIJWT(siteURL, userEmail) {
  const time = parseInt(String((new Date()).getTime()).substring(0, 10)); // JS natively supports more precise times
  const aud = getJWTaudFromURL(siteURL);

  const params = {
    email: 'sys-admin@sproutlabs.com.au',
    aud,
    iat: time,
    exp: time + 120, // Expires in 60mins
    adminAction: process.env.GLASSHOUSE_BACKEND_SERVICE == 'true',
  };
  if (userEmail) params.email = userEmail;
  return jwt.sign(params, apiJWT);
}

function getAuthHeader(siteURL, userEmail) {
  return `Bearer ${buildAPIJWT(siteURL, userEmail)}`;
}
// Does some retries, if allowed
function makeGHRequest({ site, path }) {
  return new Promise((resolve, reject) => {
    const token = buildAPIJWT(site);
    superagent
      .get(`${site}api/${dataType}`)
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        if (err) {
          console.error(err, res);
          reject(err);
        } else {

        }
      });
  });
}

export function resolveSite({ site, req }) {
  console.log(site, req);

  if (/localhost/.test(site) == true || site === 'http://glasshouse' || site == 'glasshouse' || ((req && req.user) && (req.user.aud == 'localhost' || req.user.aud == 'glasshouse'))) {
    return 'http://glasshouse';
  }
  let result;
  if (req && req.user && req.user.aud) {
    result = `https://${req.user.aud}`;
  }
  if (site) {
    // console.log(site);
    if (/(http|https)/.test(site)) {
      result = /^((http|https):\/\/.+?)(?:\/|$)/.exec(site)[1];
    } else {
      result = `https://${site}.sproutlabs.com.au`;
    }
  }


  debug('site is', result);
  return result;
}

export function create({ site, dataType, data, req }) {

}
// For api-gateway we have a token already with the aud
// for outcomes we have a siteURL
// Might have a full site url
// Might have only the start
export function getOne({ site, query }) {
}

export async function getll({ dataType, query }) {
  await makeGHRequest({ site, path });
}


// const requestOptions = { url: `${ghBase(req)}/api/${dataType}`, json: true };
// if (query) requestOptions.qs = query;
// debug('get all', requestOptions);
// const res = await client.get(requestOptions);
// debug('get all got a response for', dataType, res);
// return normalise(res);


export function getGraphQLClient({ site, userEmail }) {
  if (!site) throw new Error('Need a site to make request against');
  // The base config
  const config = {
    uri: 'https://api.sproutlabs.com.au/api/graphql',
    opts: {
      credentials: 'same-origin',
    },
  };
  if (process.env.GRAPHQL_URI_OVERRIDE) config.uri = process.env.GRAPHQL_URI_OVERRIDE;
  if (apiJWT) {
    const siteURL = resolveSite({ site });
    config.opts.headers = config.opts.headers || {};
    config.opts.headers.Authorization = getAuthHeader(siteURL, userEmail);
  }
  if (global.window && global.window.graphQLURL) config.opts.uri = global.window.graphQLURL;

  debug('GraphQL config is', config);
  return new ApolloClient({
    networkInterface: createNetworkInterface(config),
  });
}

export async function getMember({ email, site }) {
  if (!site) throw new Error('Need a site to get the memeber from');
  const client = getGraphQLClient({ site });
  const result = await client.query({
    query: gql`query getMember($emailAddress: String!) {
            member (emailAddress: $emailAddress) {
                id,
            }}`,
    variables: { emailAddress: email },
  });
  debug('Got the member', result, 'for the email', email);
  return result.data.member;
}
