export function emailFromStatement(statement) {
  return /mailto:(.+)/.exec(statement.actor.mbox)[1];
}
