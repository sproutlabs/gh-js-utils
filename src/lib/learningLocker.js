import { toLower } from 'lodash';

export function actorEmailFromStatement(statement) {
  const { mbox } = statement.actor;

  return toLower(mbox.split('mailto:')[1]);
}
