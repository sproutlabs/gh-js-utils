import { isEqual, isArray, difference, pick } from 'lodash';

function stripQuery(query) {
  return query.replace(/\s/g, '').replace(/,/g, '').replace(/__typename/g, '');
}
export function matchGraphQLQuery(toMatch) {
  const flatMatch = pick({ ...toMatch, query: stripQuery(toMatch.query) }, ['query', 'variables']);
  return function (body) {
    try {
      let bodyToUse = body;
      if (isArray(body)) {
        bodyToUse = body[0];
      }
      bodyToUse = pick(bodyToUse, ['query', 'variables']);
      const flatBody = { ...bodyToUse, query: stripQuery(bodyToUse.query) };
      const result = isEqual(flatBody, flatMatch);
      if (!result) console.log('Request body does not match', flatBody, flatMatch, 'diff', difference(flatBody.query.split(''), flatMatch.query.split('')));
      return result;
    } catch (e) {
      console.error(e);
    }
  };
}
