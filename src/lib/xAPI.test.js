import { emailFromStatement } from './xAPI';

test('Should get a email address from a xAPI statement', async () => {
  const statement = {
    actor: {
      mbox: 'mailto:test@sproutlabs.com.au',
    },
  };

  expect(emailFromStatement(statement)).toBe('test@sproutlabs.com.au');
});
