import { resolveSite, getJWTaudFromURL } from './ghAPI';

test('Resolves site URL from req', () => {
  const req = { user: { aud: 'iponz-socrates.sproutlabs.com.au' } };
  let result = resolveSite({ req });
  expect(result).toBe('https://iponz-socrates.sproutlabs.com.au');

  req.user.aud = 'glasshouse';
  result = resolveSite({ req });
  expect(result).toBe('http://glasshouse');
});

test('Resolves the site URL from other URLs', () => {
  let site = 'https://iponz-socrates.sproutlabs.com.au/admin/pages';
  let result = resolveSite({ site });
  expect(result).toBe('https://iponz-socrates.sproutlabs.com.au');

  site = 'http://localhost/home';
  result = resolveSite({ site });
  expect(result).toBe('http://glasshouse');
});

test('Resolves the site from a subdomain', () => {
  let site = 'iponz-socrates';
  let result = resolveSite({ site });
  expect(result).toBe('https://iponz-socrates.sproutlabs.com.au');

  site = 'glasshouse';
  result = resolveSite({ site });
  expect(result).toBe('http://glasshouse');
});

test('Resolves the site from a xapi object id', () => {
  const site = 'https://iponz-socrates.sproutlabs.com.au/special-technical-features-2/:803:990';
  const result = resolveSite({ site });

  expect(result).toBe('https://iponz-socrates.sproutlabs.com.au');
});

test('Gets the audience', () => {
  const result = getJWTaudFromURL('https://iponz-socrates.sproutlabs.com.au');
  expect(result).toBe('iponz-socrates.sproutlabs.com.au');
});

test('Gets the audience from a full URL', () => {
  const site = 'https://iponz-socrates.sproutlabs.com.au/api/page/145';
  const siteURL = resolveSite({ site });

  expect(siteURL).toBe('https://iponz-socrates.sproutlabs.com.au');

  const aud = getJWTaudFromURL(siteURL);
  expect(aud).toBe('iponz-socrates.sproutlabs.com.au');
});

test('Gets a GraphQL client on the web');
test('Gets a GraphQL client in Node env');
test('Build a Authorization header value on the web');
test('Build a Authorization header value in Node env');
