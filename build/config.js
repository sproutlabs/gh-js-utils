"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
const apiJWT = process.env.GLASSHOUSE_JWT_SECRET;

exports.apiJWT = apiJWT;