'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.actorEmailFromStatement = actorEmailFromStatement;

var _lodash = require('lodash');

function actorEmailFromStatement(statement) {
  const mbox = statement.actor.mbox;


  return (0, _lodash.toLower)(mbox.split('mailto:')[1]);
}