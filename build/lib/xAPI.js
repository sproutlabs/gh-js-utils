"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.emailFromStatement = emailFromStatement;
function emailFromStatement(statement) {
  return (/mailto:(.+)/.exec(statement.actor.mbox)[1]
  );
}