'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.matchGraphQLQuery = matchGraphQLQuery;

var _lodash = require('lodash');

function stripQuery(query) {
  return query.replace(/\s/g, '').replace(/,/g, '').replace(/__typename/g, '');
}
function matchGraphQLQuery(toMatch) {
  const flatMatch = (0, _lodash.pick)(_extends({}, toMatch, { query: stripQuery(toMatch.query) }), ['query', 'variables']);
  return function (body) {
    try {
      let bodyToUse = body;
      if ((0, _lodash.isArray)(body)) {
        bodyToUse = body[0];
      }
      bodyToUse = (0, _lodash.pick)(bodyToUse, ['query', 'variables']);
      const flatBody = _extends({}, bodyToUse, { query: stripQuery(bodyToUse.query) });
      const result = (0, _lodash.isEqual)(flatBody, flatMatch);
      if (!result) console.log('Request body does not match', flatBody, flatMatch, 'diff', (0, _lodash.difference)(flatBody.query.split(''), flatMatch.query.split('')));
      return result;
    } catch (e) {
      console.error(e);
    }
  };
}