'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMember = exports.getll = undefined;

let getll = exports.getll = (() => {
  var _ref = _asyncToGenerator(function* ({ dataType, query }) {
    yield makeGHRequest({ site, path });
  });

  return function getll(_x) {
    return _ref.apply(this, arguments);
  };
})();

// const requestOptions = { url: `${ghBase(req)}/api/${dataType}`, json: true };
// if (query) requestOptions.qs = query;
// debug('get all', requestOptions);
// const res = await client.get(requestOptions);
// debug('get all got a response for', dataType, res);
// return normalise(res);


let getMember = exports.getMember = (() => {
  var _ref2 = _asyncToGenerator(function* ({ email, site }) {
    if (!site) throw new Error('Need a site to get the memeber from');
    const client = getGraphQLClient({ site });
    const result = yield client.query({
      query: _graphqlTag2.default`query getMember($emailAddress: String!) {
            member (emailAddress: $emailAddress) {
                id,
            }}`,
      variables: { emailAddress: email }
    });
    debug('Got the member', result, 'for the email', email);
    return result.data.member;
  });

  return function getMember(_x2) {
    return _ref2.apply(this, arguments);
  };
})();

exports.getJWTaudFromURL = getJWTaudFromURL;
exports.resolveSite = resolveSite;
exports.create = create;
exports.getOne = getOne;
exports.getGraphQLClient = getGraphQLClient;

var _superagent = require('superagent');

var _superagent2 = _interopRequireDefault(_superagent);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _config = require('../config');

require('isomorphic-fetch');

var _apolloClient = require('apollo-client');

var _apolloClient2 = _interopRequireDefault(_apolloClient);

var _graphqlTag = require('graphql-tag');

var _graphqlTag2 = _interopRequireDefault(_graphqlTag);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const debug = (0, _debug2.default)('gh:utils:ghAPI');

if (!global.window) {
  if (!process.env.GLASSHOUSE_JWT_SECRET) {
    console.warn('You are in a Node env and the env variable GLASSHOUSE_JWT_SECRET has not been set');
  }
  if (!process.env.GLASSHOUSE_BACKEND_SERVICE) {
    console.warn('Looks like you are in a node environment but GLASSHOUSE_BACKEND_SERVICE is not set ');
  }
}
function getJWTaudFromURL(siteURL) {
  return (/^((?:http|https):\/\/(.+?))(\/|$)/.exec(siteURL)[2]
  );
}
function buildAPIJWT(siteURL, userEmail) {
  const time = parseInt(String(new Date().getTime()).substring(0, 10)); // JS natively supports more precise times
  const aud = getJWTaudFromURL(siteURL);

  const params = {
    email: 'sys-admin@sproutlabs.com.au',
    aud,
    iat: time,
    exp: time + 120, // Expires in 60mins
    adminAction: process.env.GLASSHOUSE_BACKEND_SERVICE == 'true'
  };
  if (userEmail) params.email = userEmail;
  return _jsonwebtoken2.default.sign(params, _config.apiJWT);
}

function getAuthHeader(siteURL, userEmail) {
  return `Bearer ${buildAPIJWT(siteURL, userEmail)}`;
}
// Does some retries, if allowed
function makeGHRequest({ site, path }) {
  return new Promise((resolve, reject) => {
    const token = buildAPIJWT(site);
    _superagent2.default.get(`${site}api/${dataType}`).set('Authorization', `Bearer ${token}`).end((err, res) => {
      if (err) {
        console.error(err, res);
        reject(err);
      } else {}
    });
  });
}

function resolveSite({ site, req }) {
  console.log(site, req);

  if (/localhost/.test(site) == true || site === 'http://glasshouse' || site == 'glasshouse' || req && req.user && (req.user.aud == 'localhost' || req.user.aud == 'glasshouse')) {
    return 'http://glasshouse';
  }
  let result;
  if (req && req.user && req.user.aud) {
    result = `https://${req.user.aud}`;
  }
  if (site) {
    // console.log(site);
    if (/(http|https)/.test(site)) {
      result = /^((http|https):\/\/.+?)(?:\/|$)/.exec(site)[1];
    } else {
      result = `https://${site}.sproutlabs.com.au`;
    }
  }

  debug('site is', result);
  return result;
}

function create({ site, dataType, data, req }) {}
// For api-gateway we have a token already with the aud
// for outcomes we have a siteURL
// Might have a full site url
// Might have only the start
function getOne({ site, query }) {}

function getGraphQLClient({ site, userEmail }) {
  if (!site) throw new Error('Need a site to make request against');
  // The base config
  const config = {
    uri: 'https://api.sproutlabs.com.au/api/graphql',
    opts: {
      credentials: 'same-origin'
    }
  };
  if (process.env.GRAPHQL_URI_OVERRIDE) config.uri = process.env.GRAPHQL_URI_OVERRIDE;
  if (_config.apiJWT) {
    const siteURL = resolveSite({ site });
    config.opts.headers = config.opts.headers || {};
    config.opts.headers.Authorization = getAuthHeader(siteURL, userEmail);
  }
  if (global.window && global.window.graphQLURL) config.opts.uri = global.window.graphQLURL;

  debug('GraphQL config is', config);
  return new _apolloClient2.default({
    networkInterface: (0, _apolloClient.createNetworkInterface)(config)
  });
}